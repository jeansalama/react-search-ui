# A simple react search ui for Elastic Workplace Search

## Requirements

Assuming you have :
- an instance of Confluence or Jira
- an instance of Elastic Enterprise Search configured to sync with at least
Confluence or Jira
- Some indexed contents from Confluence and Jira

## Installation

Install dependencies with `npm`

```sh
npm install
```

## Prepare .env variables

- At project's root, create a new file `.env` with the same variables 
  from `.env.template` and fill it with the suitable values for ur environment.
  For instance,

```properties
EES_URL="https://your-enterprise-search.com"
CLIENT_ID="***Your Client ID from Workplace search OAuth settings****"
ACCESS_TOKEN"****Your Access Token from the redirected url, see the steps below****"
```

### Get manually your access token

- Go to Elastic Workplace search admin UI

- Follow the steps for [configuring OAuth](https://www.elastic.co/guide/en/workplace-search/current/building-custom-search-workplace-search.html#configuring-search-oauth) ; you will get the client ID from this configuration

- and [get access token through implicit OAuth flow](https://www.elastic.co/guide/en/workplace-search/current/building-custom-search-workplace-search.html#authenticating-search-user-implicit)

- you will end with a request url like this :

```
# Pattern
<Your Ent. url host>/ws/oauth/authorize?response_type=token&client_id=<Client ID from 1st step>&redirect_uri=<your spa url configured in 1st step>

# Example
https://enterprise.fondation-recherche.dev.desjardins.com/ws/oauth/authorize?response_type=token&client_id=<Client ID from 1st step>&redirect_uri=https://localhost:4001

```

- The redirected url contains a valid access token for 2 hours only, copy it and replace its value in `.env` or in Liferay's webcomponent config.

- You can use CURL to get easily your access token

```sh
export EES_URL="https://your-enterprise-search.com"
export CLIENT_ID="***Your Client ID from Workplace search OAuth settings****"
export REDIRECT_URI="https://your-redirect-uri.com"
export USER="your elastic user"
export PASSWORD="your password"

curl -Iks -o /dev/null "${EES_URL}/ws/oauth/authorize?response_type=token&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}" \
  -u "${USER}:${PASSWORD}" \
  -w %{redirect_url} \
  | grep  -oE "#.*" | cut -d '&' -f 1 | cut -d '=' -f 2
```



## Build the search ui

Since the app uses env variables and webpacks, you will need to build it before
running it. You will need to rebuild it if you change any value of `.env`
variables.

```sh
npm run build
```

## Run the search ui

```sh
npm run dev
```