#!/bin/bash

# export CI_PATH_FPCI="/java/workspace/fpci"
# export CI_PATH_FPSCI="/java/workspace/fpsci"

# fly --target fpsci login --team-name fondation-portail-samples --concourse-url https://ci.cfzcea.dev.desjardins.com


# Candidate
fly -t fpsci set-pipeline -c ${CI_PATH_FPCI}/pipelines/webcomponent.yml -p react-search-ui-candidate-72 -l ${CI_PATH_FPCI}/params/params.yml -l ${CI_PATH_FPSCI}/params/params-fps.yml -l ./ci/params.yml

#destroy candidate pipeline
#fly -t fpsci destroy-pipeline -p react-search-ui-candidate-72

# Check resource
fly -t fpsci check-resource --resource react-search-ui-candidate-72/fpci

########################################################################################################################################################################
########################################################################################################################################################################
########################################################################################################################################################################

# Release
fly -t fpsci set-pipeline -c ${CI_PATH_FPCI}/pipelines/release-composante.yml -p react-search-ui-release-72 --var "artefact=fpsc-poc-webcomponent-sommaire" --var "version=1.1.13-build.1" -l ${CI_PATH_FPCI}/params/params.yml -l ${CI_PATH_FPSCI}/params/params-fps.yml -l ./ci/params.yml

#destroy release pipeline
#fly -t fpsci destroy-pipeline -p react-search-ui-release-72

# Check resource
fly -t fpsci check-resource --resource react-search-ui-release-72/fpci