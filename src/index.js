import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './app/App';


export default class ReactSearchUi extends React.Component {
    render() {
        return(
            <div>
                <App/>
            </div>
        );
    }
  }




if(!customElements.get('react-search-ui')) { 
    customElements.define('react-search-ui',
        class extends HTMLElement {
            constructor() {
                super();
            }

            createComponent(router_base_page) {
                return React.createElement(ReactSearchUi, { router_base_page });
            }

            connectedCallback() {
                ReactDOM.render(this.createComponent(this.getAttribute('router-base-page')), this); 
            }
        }
    ); 
}