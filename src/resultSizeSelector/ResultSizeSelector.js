import React from 'react';
import { resultsSize } from '../constants/Constants';

export class ResultSizeSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSize: resultsSize[0]
        };
    }

    handleSelect() {
        var size = document.getElementById(this.props.id).value;
        size = parseInt(size);
        this.props.handleChange(size);
        console.log('[ResultSizeSelector] Change results size to :', size);
        this.setState({
            currentSize: size
        });
    }

    render () {
        var options = resultsSize.map(
            (size, index) => <option value={size} key={index}>{size}</option>
        );
        return (
            <div className="input-group input-group-sm">
                <label 
                    className="input-group-text" 
                    htmlFor={this.props.id}>
                        Résultats par page
                </label>
                <select 
                    id={this.props.id}
                    className="form-select"
                    onChange={() => this.handleSelect()}
                    value={this.props.size}
                >
                    {options}
                </select>
            </div>
        );
    }
}