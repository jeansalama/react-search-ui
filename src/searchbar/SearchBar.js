import React from "react"

export class SearchBar extends React.Component {
    render() {
        return (
            <form onSubmit={(e) => this.props.handleSubmit(e)}>
                <div className="form-row">
                    <div className="form-group search-bar">
                        <label htmlFor="header-search">
                            <span hidden>Recherche multi-sources</span>
                        </label>
                        <div className="input-group">
                            <input
                                type="text"
                                id="header-search"
                                className="form-control"
                                placeholder="recherche multi-sources..."
                                name="q"
                                value={this.props.value}
                                onChange={(e) => this.props.handleChange(e)}
                            />
                            <div className="input-group-append">
                                <button type="submit"
                                    className="btn btn-success rounded-0 rounded-end">
                                    Rechercher
                                    <svg 
                                        className="bi bi-search"
                                        width="1.5em"
                                        height="1.5em"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path fillRule="evenodd" d="M12.442 12.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clipRule="evenodd"></path>
                                        <path fillRule="evenodd" d="M8.5 14a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM15 8.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clipRule="evenodd"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
        );
    }
}