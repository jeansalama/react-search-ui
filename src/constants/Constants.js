export const client_id = process.env.CLIENT_ID;
export const ees_url = process.env.EES_URL;
export const access_token = process.env.ACCESS_TOKEN;

export const endpoint = ees_url + '/api/ws/v1/search';

export const resultsSize = [5, 10, 15, 20];

export const pageMin = 1;

export const maxContentLength = 150;