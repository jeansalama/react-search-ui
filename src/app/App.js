import React from "react";

import { ErrorAlert } from "../alerts/ErrorAlert";
import { MessageAlert } from "../alerts/MessageAlert";
import { WarningAlert } from "../alerts/WarningAlert";
import { SuccessAlert } from "../alerts/SuccessAlert";
import { ResultsList } from "../results/ResultsList";
import { ResultSizeSelector } from '../resultSizeSelector/ResultSizeSelector';
import { Pagination } from "../Pagination/Pagination.js";
import { SearchBar } from "../searchbar/SearchBar";
import { 
    access_token,
    endpoint,
    resultsSize,
} from '../constants/Constants';

import './App.css';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error : null,
			isLoaded: false,
            isLoading: false,
            isEmptyResults: false,
            query: '',
            results: [],
            resultsSizeIndex: 0,
            size: resultsSize[0],
            currentPage: 0,
            totalPages: 0,
            totalResults: 0
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
    }

    handleChange(event) {
        this.setState({
            query: event.target.value,
            isLoaded: false,
            isEmptyResults: false,
        });
    }

    handleSubmit(event) {
        this.setState({
            isLoaded: false,
            isLoading: true,
            isEmptyResults: false,
            currentPage: 1
        });
        console.log('Query on submit : ', this.state.query);
        this.sendQuery(this.state.query, this.state.size, 1);
        event.preventDefault();
    }

    handlePagination(page) {
        this.setState({
            currentPage: page
        });
        console.info('Select page ', page, ' with query ', this.state.query);
        this.sendQuery(this.state.query, this.state.size, page);
    }

    handleSelect(size) {
        this.setState({
            size: size,
            currentPage: 1
        });
        console.info('Select size ', size, ' with query ', this.state.query);
        this.sendQuery(this.state.query, size, 1);
    }

    sendQuery(query, size, current) {
        fetch(endpoint, {
                method: 'post',
                headers: {
                    "Access-Control-Allow-Origin": window.location.origin,
                    "Authorization": "Bearer " + access_token,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    "query": query,
                    "page": {
                        "size": size || this.state.size,
                        "current": current || this.state.currentPage
                    }
                })
            }
        )
        .then(res => res.json())
        .then(
            (result) => {
                console.log('result : ', result);
                if (!result.errors) {
                    this.setState({
                        isLoaded: true,
                        isLoading: false,
                        isEmptyResults: result.results.length === 0,
                        error: '',
                        results: result.results,
                        totalPages: result.meta.page.total_pages,
                        totalResults: result.meta.page.total_results
                    });
                } else {
                    console.error('error :', result.errors);
                    this.setState({
                        error: "Erreur avec la requête ! Recommencez plus tard",
                        isLoaded: false,
                        isEmptyResults: false
                    });
                }
            }
        )
    }

    render() {
        const { 
            error,
            isLoaded,
            isLoading,
            isEmptyResults,
            query,
            results,
            totalResults
        } = this.state;

        return(
            <div className="container shadow my-2 py-2">
                <h2 className="text-success">Recherche multi-sources</h2>
                
                <div className="m-2">
                    <SearchBar
                        value={ this.state.value }
                        handleChange={ (e) => this.handleChange(e) }
                        handleSubmit={ (e) => this.handleSubmit(e) }
                    />
                </div>

                <ErrorAlert
                    error={ error }
                    isHidden={ !error }
                />

                <MessageAlert
                    message={'Chargement des résultats...'}
                    isHidden={ !isLoading || error }
                />

                <WarningAlert
                    message={'Aucun résultat trouvé pour "' + query + '"'}
                    isHidden={ !isEmptyResults }
                />

                <SuccessAlert
                    message={totalResults + ' résultat(s) trouvé(s)'}
                    isHidden={ !isLoaded || error || isEmptyResults }
                />
                

                <div className="my-3 search-results">
                    <div className="container">
                        <div className="row justify-content-between py-2">
                            <div className="col col-auto form-group">
                                <ResultSizeSelector
                                    id = 'top-select-results-size'
                                    size = {this.state.size}
                                    handleChange = {(i) => this.handleSelect(i)}
                                />
                            </div>

                            <div className='col col-auto' hidden={results.length == 0}>
                                <div className="row justify-content-end">
                                    <Pagination
                                        totalPages={this.state.totalPages}
                                        currentPage={this.state.currentPage}
                                        handlePagination={this.handlePagination}
                                        isHidden={results.length == 0}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <ResultsList
                        results={results}
                    />
                </div>
            </div>
        );
    }
  }
