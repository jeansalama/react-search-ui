import React from "react";
import { maxContentLength } from "../constants/Constants";
import unknown from '../../images/unknown.svg';
import confluence from '../../images/confluence.svg';
import jira from '../../images/jira.svg';

export class ResultItem extends React.Component {
	
	getPathIcon(source) {
		var path='';
		if (!source || source == undefined) {
			path=unknown;
		} else if (source.includes('confluence')) {
			path=confluence;
		} else if (source.includes('jira')) {
			path=jira;
		}
		return path;
	}
	
	getSubstring(str, size) {
		str = (str !== undefined && str !== null ? str : '');
		var min_size = Math.min(size, str.length);
		return str.substr(0, min_size);
	}
	
	render() {
		return (
			<li className="list-group-item border-0 d-flex rounded shadow m-2 p-0 input-group">
				<div className="image input-group-text py-4 m-0">
					<img src={this.getPathIcon(this.props.source)}
							className="img-fluid"
					></img>
				</div>
				<div className="form-control">
					<a href={this.props.url} 
						className="h4 text-success text-decoration-none break"
					>
						{this.props.title}
					</a>
					<p className="text-muted">
						{this.getSubstring(this.props.content, maxContentLength)}
					</p>
					<p><a className="break" href={this.props.url}>{this.props.url}</a></p>
				</div>               
				
			</li>
	  
	  	);
	}
}