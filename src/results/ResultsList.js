import React from "react";
import { ResultItem } from "../results/ResultItem";

export class ResultsList extends React.Component {
    render() {
        var resultsList = this.props.results.map(
			(result, index) => 
                <ResultItem 
                    key = { index }
                    title = { result.title.raw }
                    source = { result.source.raw }
                    url = { result.url.raw }
                    content = { result.body !== undefined ? result.body.raw : '' }
                />
		);

        return (
            <ul id="results-list" className="list-group">
                { resultsList }
            </ul>
        );
    }
}
