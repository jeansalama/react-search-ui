import React from "react";
import ReactPaginate from 'react-paginate';

export class Pagination extends React.Component {
    renderPreviousLabel() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" 
                fill="currentColor" className="bi bi-chevron-left" viewBox="0 0 16 16">
                <path fillRule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
            </svg>
        );
    }

    renderNextLabel() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                fill="currentColor" className="bi bi-chevron-right" viewBox="0 0 16 16">
                <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
            </svg>
        );
    }

    renderDots() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
            fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
            </svg>
        );
    }


    render() {
        return (
            <ReactPaginate
                previousLabel={ this.renderPreviousLabel() }
                nextLabel={ this.renderNextLabel() }
                breakLabel={ this.renderDots() }
                breakClassName={'page-item'}
                breakLinkClassName={'page-link text-success'}
                containerClassName={'pagination pagination-sm'}
                pageClassName={'page-item'}
                pageLinkClassName={'page-link link-success'}
                activeLinkClassName={'bg-success text-white border-success'}
                previousClassName={'page-item'}
                previousLinkClassName={'page-link text-success'}
                nextClassName={'page-item'}
                nextLinkClassName={'page-link text-success'}
                disabledClassName={'disabled'}
                pageCount={this.props.totalPages}
                forcePage={this.props.currentPage - 1}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={(data) => this.props.handlePagination(data.selected + 1)}
                activeClassName={'active'}
                hidden={this.props.isHidden}
            />
        );
    }
}